<?php
/* Template Name: Meeting Event Template */ get_header('image'); ?>

<main role="main" class="col-md-12">
	<div class="container text-center">
		<!-- container -->
		<!-- section -->
		<section>
			<h1 class="title text-center"><?php the_title(); ?></h1>
			<?php
			$args = array(
				'post_type' => 'meetings-events'
			);
			query_posts($args);
			if (have_posts()): while (have_posts()) : the_post(); ?>

			<div class="box-container col-md-6">
				<div class="room-thumb thumbnail">
					<article id="post-<?php the_ID(); ?>" <?php post_class( 'rooms-post'); ?>>
						<div class="thumb">
							<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
								<?php the_post_thumbnail('gallery-slide'); // Declare pixel size you need inside the array ?>
							<?php endif; ?>
						</div>
						<div class="box-text text-center">
							<h4><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h4>
							<?php the_excerpt(); ?>
						</div>
					</article>
				</div>
			</div>

			<?php endwhile; ?>

			<?php else: ?>

			<!-- article -->
			<article>
				<h2>
					<?php _e( 'Sorry, nothing to display.', karisma_text_domain ); ?>
				</h2>
			</article>
			<!-- /article -->

			<?php endif; ?>

			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
	</div>
	<!-- end container -->
</main>

<?php get_footer(); ?>
