<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>
	<meta name="description" content="<?php bloginfo('description'); ?>">
	<?php wp_head(); ?>

	<script type="text/javascript">
		jQuery(window).scroll(function(){
			if (jQuery(this).scrollTop() > 70) {
				jQuery('#mainNav').addClass('navbar-fixed-top');
				jQuery('.navbar-brand').css({'display':'block'});
			} else {
				jQuery('#mainNav').removeClass('navbar-fixed-top');
				jQuery('.navbar-brand').css({'display':'none'});
			}
		});
	</script>

</head>
<body <?php body_class(); ?>>
	<!-- header -->
	<header id="homeSliderCarousel" class="carousel slide header-home clear" role="banner">

		<div class="wrap-logo">
			<div class="logo">
				<?php krs_headlogo(); ?>
				<?php if (ot_get_option('krs_head_hotelinfo_actived') == 'on'): ?>
					<div class="header-address">
						<span> <i class="fa fa-map-marker"></i> <?php echo ot_get_option('krs_address'); ?></span>
						<span> <i class="fa fa-phone"></i> <?php echo ot_get_option('krs_phone'); ?></span>
						<span><i class="fa fa-envelope"></i> <a href="mailto:<?php echo ot_get_option('krs_email'); ?>"><?php echo ot_get_option('krs_email'); ?></a></span>
					</div>
				<?php endif; ?>
			</div>
		</div>

			<!-- nav -->
		<nav id="mainNav" class="navbar navbar-custom">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
					</button>
					<!-- <div class="box-logo">
					<?php //krs_headlogo(); ?>
					</div> -->
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<?php karisma_nav(); ?>
				</div>
			</div>
		</nav>
		<!-- /nav -->
		<!-- Wrapper for Slides -->

		<div class="carousel-inner">
			<?php
			if ( function_exists( 'ot_get_option' ) ) {
				$images = explode( ',', ot_get_option( 'krs_gallery', '' ) );
				if ( ! empty( $images ) ) {
					$i=0;
					foreach( $images as $id ) {
						if ( ! empty( $id ) ) {
							if($i++ == 0) {
								$active = 'active';
							} else {$active = '';}
							$full_img_src = wp_get_attachment_image_src( $id, 'gallery-slide-main' );
							$thumb_img_src = wp_get_attachment_image_src( $id, 'gallery-slide-main' );
							?>
							<div class="item <?php echo $active; ?>">
								<div class="home-slider" style="background-image:url('<?php echo $full_img_src[0]; ?>');"></div>
							</div>
							<?php
						}
					}
				}
			}
			?>
			<a class="left carousel-control" href="#homeSliderCarousel" data-slide="prev"><span class="icon-prev"></span></a>
			<a class="right carousel-control" href="#homeSliderCarousel" data-slide="next"><span class="icon-next"></span></a>
		</div>

		<section class="booking-section">
			<div class="booking-box">
				<?php do_shortcode("[booking_engine]"); ?>
			</div>
		</section>


	</header>
	<!-- /header -->
