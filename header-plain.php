<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>
	<meta name="description" content="<?php bloginfo('description'); ?>">
	<?php wp_head(); ?>

	<!-- <script type="text/javascript">
		jQuery(window).scroll(function(){
			if (jQuery(this).scrollTop() > 80) {
				jQuery('#mainNav').addClass('navbar-fixed-top');
			} else {
				jQuery('#mainNav').removeClass('navbar-fixed-top');
			}
		});
	</script> -->
</head>
<body <?php body_class(); ?>>
	<!-- header -->
	<header class="header-image-page plain clear" role="banner">
			<!-- nav -->
		<nav id="mainNav" class="navbar navbar-custom navbar-fixed-top nav-background">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
					</button>
					<div class="box-logo">
					<?php krs_headlogo(); ?>
					</div>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<?php karisma_nav(); ?>
				</div>
			</div>
		</nav>
		<!-- /nav -->

	</header>
	<!-- /header -->
