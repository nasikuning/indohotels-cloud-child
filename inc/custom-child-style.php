<?php /*
$mainColor = Main Color
$seColor = Secondary Color
$terColor = tertiary Color

== Change Body typography ==

$font_body =  $fontsColor['font-family']
$fontsColor['font-color'] = Body typography Color
$fontHover = Hover color text

== Nav Stylish ==

 $navStyle['font-color']
 $navStyle['font-family']
 $navStyle['font-size']
 $navStyle['font-style']
 $navStyle['font-weight']

== Nav Background ==

 $navBg['background-color']
 $navBg['background-repeat']
 $navBg['background-attachment']
 $navBg['background-position']
 $navBg['background-size']
 $navBg['background-image']

== Main Background ==

 $mainBg['background-color']
 $mainBg['background-repeat']
 $mainBg['background-attachment']
 $mainBg['background-position']
 $mainBg['background-size']
 $mainBg['background-image']

== Home Background ==

 $section3bg['background-color']
 $section3bg['background-repeat']
 $section3bg['background-attachment']
 $section3bg['background-position']
 $section3bg['background-size']
 $section3bg['background-image']

== Footer Background ==

 $footerBg['background-color']
 $footerBg['background-repeat']
 $footerBg['background-attachment']
 $footerBg['background-position']
 $footerBg['background-size']
 $footerBg['background-image']

 == Footer color ==

  $footerStyle['font-color']

*/?>
<style id="indohotels-style" type="text/css">

	::selection {
		background: <?php echo $mainColor; ?>;
	}

	.nav-background,
	.nav-no-image {
		background-color: <?php echo $navBg['background-color']; ?>;
		color: <?php echo  $navStyle['font-color']; ?>
	}

	.navbar-fixed-top {
		background: <?php echo $mainColor; ?>;
	}

	button,
	html input[type=button],
	input[type=reset],
	input[type=submit] {
		border: 1px solid <?php echo $mainColor; ?>;
		background-color: <?php echo $mainColor; ?>;
	}

	body {
		font-family: <?php echo isset($font_body) ? $font_body : ''; ?>;
		color: <?php echo isset($fontsColor['font-color']) ? $fontsColor['font-color'] : ''; ?>;
	}

	/* NAVIGATION */
	.nav-no-image,
	.nav ul.dropdown-menu {
		background-color: <?php echo $mainColor; ?>;
	}
	.nav-background {
		background-color: <?php echo $navBg['background-color']; ?>;
		color: <?php echo $navStyle['font-color']; ?>;
	}
	nav .navbar-custom {
		background-color: <?php echo $terColor; ?>;
	}
	.nav>li>a {
	  color: <?php echo $navStyle['font-color']; ?>;
		font-size: <?php echo $navStyle['font-size']; ?>;
	}
	/*@media(min-width: 768px){
		.navbar-header {
			border-bottom: 5px solid <?php //echo $mainColor; ?>;
		}
	}*/

	/* HEADER */
	@media(min-width:768px){
		.header-image-page.plain #mainNav {
			background-color: <?php echo $navBg['background-color']; ?>;
		}
	}
	@media (max-width: 768px) {
		.navbar .navbar-collapse {
			background-color: <?php echo $mainColor; ?>;
		}
	}

	@media(max-width:767px){
		.header-image-page.plain #mainNav button {
			color: <?php echo $mainColor; ?>
		}

	  .navbar-collapse.in #menu-primary-menu {
			background-color: <?php echo $navBg['background-color']; ?>;
			color: <?php echo $navStyle['font-color']; ?>;
			margin-top: 0;
	  }
		.header-image-page.plain .nav-background {
			background-color: transparent;
		}
	}

	.book-room,
	.home-gallery .title-gallery-home span.line-color,
	.contact-headline,
	.btn-check,
	.btn-check:active {
		background-color: <?php echo $mainColor; ?>
	}
	.btn-check:hover {
		background-color: <?php echo $seColor; ?>;
	}
	.btn-outline:hover,
	.btn-outline:focus,
	.btn-outline:active,
	.btn-outline.active,
	.book-room:hover,
	.book-room:hover a,
	.owl-prev .glyphicon.glyphicon-chevron-left,
	.owl-next .glyphicon.glyphicon-chevron-right {
		color: <?php echo $seColor; ?>;
	}

	/* BOOKING FORM */
	.booking .form-control,
	.booking .form-control[readonly],
	.booking .input-group-addon {
		border: 1px solid <?php echo $mainColor; ?>;
	}

	.box-gallery {
		border-top: 2px solid <?php echo $mainColor; ?>;
		border-bottom: 2px solid <?php echo $mainColor; ?>;
	}

	/* CONTACT */
	.info-text .info-icon .icon-round {
		background: <?php echo $seColor; ?>;
	}

	/* HEADING */
	h1.title,
	#welcome-text h1,
	#room-section .box-bg h2,
	#room-section .box-bg h4,
	.home-facility h2,
	body.home .deals h3 {
		color: <?php echo $mainColor; ?>;
	}

	main[role="main"] {
		background-image: url(<?php echo $mainBg['background-image']; ?>);
		background-repeat: <?php echo $mainBg['background-repeat']; ?>;
		background-color: <?php echo $mainBg['background-color']; ?>;
	}

	@media(max-width: 767px){
		.navbar-toggle {
			color: <?php echo $terColor; ?>;
		}
		.booking-section {
			background-color: <?php echo $mainColor; ?>;
		}
	}

	#ros-in .box-room {
		background-color: <?php echo $terColor; ?>;
	}

	.booking .booking-title,
	#ros-in h2 {
		color:#fff;
	}

	.home-carousel {
		border-bottom: 1px solid <?php echo $mainColor; ?>;
	}

	.box-home-grid .overlay {
		background-color: <?php echo $terColor; ?>;
	}

	/* FACILITIES */
	.box h4 {
		background-color: <?php echo $mainColor; ?>;
	}
	.box-home-grid {
		background-color: <?php echo $terColor; ?>;
	}
	.hotel-info-home {
		background-image: url(<?php echo $section3bg['background-image']; ?>);
		background-repeat: <?php echo $section3bg['background-repeat']; ?>;
		background-color: <?php echo $section3bg['background-color']; ?>;
	}

	/* ROOMS */
	.room-thumb:hover,
	.room-thumb:active {
		background-color: <?php echo $mainColor; ?>;
		color: #fff;
	}
	.room-details h3.room-details-f-title {
		color: <?php echo $seColor; ?>;
	}
	h2.title-room-list a {
		color: <?php echo $mainColor; ?>;
	}
	.room-booking {
		border: 1px solid <?php echo $mainColor; ?>;
	}

	/* FOOTER */
	footer {
		clear: both;
		background-color: <?php echo isset($footerBg['background-color']) ? $footerBg['background-color'] : '#6d6e71'; ?>;
		background-image: url(<?php echo isset($footerBg['background-image']) ? $footerBg['background-image'] : ''; ?>);
		background-repeat: <?php echo isset($footerBg['background-repeat']) ? $footerBg['background-repeat'] : ''; ?>;
		border-top: 2px solid <?php echo isset($mainColor) ? $mainColor : '#000'; ?>;
	}
	.footer-info p {
		color: <?php echo $footerStyle['font-color']; ?>;
	}
	.footer-distributed a,
	.footer-distributed .footer-icons a:hover {
		color: <?php echo $mainColor; ?>;
	}
	.footer-distributed .krs_info_widget  p a {
		color: <?php echo $seColor; ?>;
	}
	.footer-distributed .krs_info_widget i {
		background-color: <?php echo $seColor; ?>;
	}
	.footer-distributed .footer-icons a {
		background-color: <?php echo $seColor; ?>;
		border: 2px solid <?php echo $mainColor; ?>;
	}
	.footer-distributed .footer-icons a:hover {
		background-color: <?php echo $mainColor; ?>;
		color: <?php echo $mainColor; ?>;
		border: 2px solid <?php echo $mainColor; ?>;
	}
	.footer-credits {
		background-color: <?php echo $terColor; ?>;
	}
	.footer-distributed .footer-links li a
	.footer-distributed .footer-links li:hover a {
		color: <?php echo $fontHover; ?>;
		text-decoration: underline;
	}

	.line {
		background: <?php echo $mainColor; ?>;
	}

	.gallery-view .slick-prev:before,
	.gallery-view .slick-next:before {
		color: <?php echo $mainColor; ?>;
	}

	/* Caleran */
	.caleran-container .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-start:not(.caleran-hovered) span, .caleran-container-mobile .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-start:not(.caleran-hovered) span, .caleran-container .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-end:not(.caleran-hovered) span, .caleran-container-mobile .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-end:not(.caleran-hovered) span, .caleran-container .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-selected, .caleran-container .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-start, .caleran-container .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-end, .caleran-container-mobile .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-selected, .caleran-container-mobile .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-start, .caleran-container-mobile .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-end {
		background-color: <?php echo $mainColor; ?> !important;
	}
	.caleran-container .caleran-input .caleran-header .caleran-header-end .caleran-header-end-day, .caleran-container .caleran-input .caleran-header .caleran-header-end .caleran-header-start-day, .caleran-container .caleran-input .caleran-header .caleran-header-start .caleran-header-end-day, .caleran-container .caleran-input .caleran-header .caleran-header-start .caleran-header-start-day, .caleran-container-mobile .caleran-input .caleran-header .caleran-header-end .caleran-header-end-day, .caleran-container-mobile .caleran-input .caleran-header .caleran-header-end .caleran-header-start-day, .caleran-container-mobile .caleran-input .caleran-header .caleran-header-start .caleran-header-end-day, .caleran-container-mobile .caleran-input .caleran-header .caleran-header-start .caleran-header-start-day {
		color: <?php echo $mainColor; ?>;
	}
	.caleran-container-mobile .caleran-input .caleran-header .caleran-header-separator {
		color: <?php echo $mainColor; ?>;
	}
	.caleran-container .caleran-input .caleran-calendar .caleran-days-container .caleran-today, .caleran-container-mobile .caleran-input .caleran-calendar .caleran-days-container .caleran-today {
		color: <?php echo $mainColor; ?>;
	}
	.caleran-container-mobile .caleran-input .caleran-footer button.caleran-cancel {
		border: 1px solid <?php echo $mainColor; ?> !important;
		color: <?php echo $mainColor; ?> !important;
	}
	.caleran-apply-d, .caleran-apply {
		background: <?php echo $mainColor; ?> !important;
  	border: 1px solid <?php echo $mainColor; ?> !important;
	}
	.caleran-container .caleran-input .caleran-calendar .caleran-days-container .caleran-day-unclick, .caleran-container-mobile .caleran-input .caleran-calendar .caleran-days-container .caleran-day-unclick {
		background: <?php echo $mainColor; ?>;
	}
</style>
