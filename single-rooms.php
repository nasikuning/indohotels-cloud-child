<?php get_header('image'); ?>

<main role="main" class="col-md-12">
	<div class="container">

		<section class="box-content">
			<?php if (have_posts()): while (have_posts()) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>">

					<div class="room-title">
						<h2><?php the_title(); ?></h2>
					</div>

					<div class="room-details">
						<div class="row">
							<div class="col-md-8 col-sm-8">
								<div class="room-image">
									<div id="slider">
											<!-- Top part of the slider -->
											<div id="carousel-bounding-box">
												<div class="carousel slide" id="myCarousel">
													<div class="carousel-inner">
														<?php
														$images = rwmb_meta( 'indohotels_imgadv', 'size=gallery-slide' );
														if ( !empty( $images ) ) {
															$i = 0;
															foreach ( $images as $image ) {
																if($i++ == 0) {
																	$active = 'active';
																} else {$active = '';}
																echo '<div id="slide-'. $i .'" class="'. $active .' item">';
																echo '<span>';
																echo '<img src="', esc_url( $image['url'] ), '"  alt="', esc_attr( $image['alt'] ), '">';
																echo '</span>';
																echo '</div>';
															}
														}
														?>
													</div><!-- end .carousel-inner -->
													<a class="carousel-control left" data-slide="prev" href="#myCarousel"><span><i class="fa fa-chevron-left" aria-hidden="true"></i></span></a>
													<a class="carousel-control right" data-slide="next" href="#myCarousel"><span><i class="fa fa-chevron-right" aria-hidden="true"></i></span></a>
												</div><!-- end .carousel-slide -->
											</div><!-- end carousel-bounding-box -->
										</div><!-- end slider-->
								</div><!-- end .room-image -->
							</div>
							<div class="col-md-4 col-sm-4">
								<div class="room-info-spec">
									<p>Room Size : <?php echo rwmb_meta( 'room_size' ); ?> m2</p>
									<p>View : <?php echo rwmb_meta( 'room_view' ); ?></p>
									<p>Ocupancy : <?php echo rwmb_meta( 'room_occupancy' ); ?> Person</p>
									<p>Bed Size : <?php echo rwmb_meta( 'bed_size' ); ?></p>
								</div>
								<div class="room-details-desc">
									<?php the_content(); ?>
								</div>

								<?php $data['propery_id'] = get_option('idn_booking_engine.propery_id'); ?>
								<div class="text-center">
									<a href="//www.indohotels.id/website/property/<?php echo $data['propery_id']; ?>" class="btn btn-check"><?php _e('Check Availability', karisma_text_domain); ?>
									</a>
								</div>
							</div>
						</div>
					</div><!-- end .room-details -->

				</article>
			<?php endwhile; ?>

			<?php else: ?>
				<article>
					<h1><?php _e( 'Sorry, nothing to display.', 'indohotels' ); ?></h1>
				</article>
			<?php endif; ?>

		</section>
	</div>
</main>

<?php get_footer(); ?>
